<?php


namespace app\index\model;


use think\Db;
use think\Model;

class Account extends Model
{

    /**查询平台**/
    public function GetAccountRow(){
        return Db::table('test_account')->field('platform')->group('platform')->select();
    }

    /**查询平台**/
    public function GetSalesGroupRow(){
        return Db::table('test_account')->field('sales_group')->group('sales_group')->select();
    }
}