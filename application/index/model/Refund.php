<?php


namespace app\index\model;


use think\Db;
use think\Model;

class Refund extends Model
{
    public function getRefund($time = '',$platform = ''){
        $where =[];

        if(!empty($time)){
            $where['time'] = ['between',$this->getthemonth($time)];
        }
        if(!empty($platform)){
            $where['platform'] = $platform;
        }
        return Db::table('test_refund')
            ->field('count(id) as value,reason as name')
            ->where($where)
            ->group('reason')
            ->select();

    }
    function getthemonth($date)//获取具体日期的函数
    {
        $firstday = date('Y-m-01', strtotime($date));
        $lastday = date('Y-m-d', strtotime("$firstday +1 month -1day"));
        return array($firstday, $lastday);
    }
}