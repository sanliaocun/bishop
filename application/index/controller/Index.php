<?php
namespace app\index\controller;


use think\Controller;
use think\Db;
use app\index\model\Account as AccountModel;
use app\index\model\Order as OrderModel;
use app\index\model\Refund as RefundModel;
use think\Request;

class Index extends Controller
{
    public function index()
    {
        $AccountModel =  new AccountModel();
        $OrderModel = new OrderModel();

        $platform = $AccountModel->GetAccountRow();
        $sales_group = $AccountModel->GetSalesGroupRow();
        $time = '2020-02';//date('Y-m');
        $orderlist = $OrderModel->getOrder('',$time);
        $time_list = $OrderModel->tiemsData($time);
        $aller = array();
        foreach ($time_list as $key=> $value){
            $aller[$key]['name'] = $value;
            $aller[$key]['sales_volume'] = 0;
            foreach ($orderlist as $ke=>$val){
                if($value == $val['payment_time']){
                    $aller[$key]['sales_volume'] = $val['sales_volume'];
                }
            }
        }
        //获取柱子
        $monthlyColumn = $OrderModel->getColumn();
        unset($monthlyColumn[0]);
        $this->assign('monthlyColumn',$monthlyColumn);
//        饼状图
        $RefundModel = new RefundModel();
        $refList = $RefundModel->getRefund();
//        多表查询
        $orderSku = $OrderModel->getClassOrderSku();
        $orderskuLists = [];
        foreach ($orderSku as $key=>$value){
            $orderskuLists[$key]['name'] = $value['name'];
            $orderskuLists[$key]['sales_volume'] = $value['sales_volume'];
            $orderskuLists[$key]['gross_profit'] = round(($value['sales_volume'] - $value['purchasing_cost'] - $value['platform_cost'] - $value['logistics_cost']),2);
            $orderskuLists[$key]['gross_profit_rate'] =  $value['sales_volume'] != 0 ?round(($orderskuLists[$key]['gross_profit'] / $value['sales_volume']) * 100,2):0;
        }
        $slalist = $OrderModel->sumSales();
        $slalist['gross_profit'] = $slalist['sales_volume'] != 0 ? round(($slalist['sales_volume'] - $slalist['purchasing_cost'] - $slalist['platform_cost'] - $slalist['logistics_cost'])/10000,2):0;
        $slalist['platform_cost'] = $slalist['platform_cost'] != 0 ? round(floor($slalist['platform_cost']) / 10000, 2) : 0;
        $slalist['logistics_cost'] = $slalist['logistics_cost'] != 0 ? round(floor($slalist['logistics_cost']) / 10000, 2) : 0;
        $slalist['sales_volume'] = $slalist['sales_volume'] != 0 ? round(floor($slalist['sales_volume']) / 10000, 2) : 0;
        $slalist['purchasing_cost'] = $slalist['purchasing_cost'] != 0 ? round(floor($slalist['purchasing_cost']) / 10000, 2) : 0;
        $slalist['gross_profit_rate'] = $slalist['gross_profit'] != 0 ? round(($slalist['gross_profit'] / $slalist['sales_volume']) * 100,2) : 0;

//        dump($orderskuLists);exit;
        $this->assign('slalist',$slalist);
        $this->assign('orderskuLists',$orderskuLists);
        $this->assign('refList',json_encode(array_values($refList)));
        $this->assign('aller',$aller);
        $this->assign('sales_group',$sales_group);
        $this->assign('platform',$platform);
        return $this->fetch('index/index');
    }
    /**
     * 格式化数字
     */
    public function float_number($number){
        dump($number);exit;
        $length = strlen($number);  //数字长度
        if($length > 8){ //亿单位
            $str = substr_replace(strstr($number,substr($number,-7),' '),'.',-1,0)."亿";
        }elseif($length >4){ //万单位
            //截取前俩为
            $str = substr_replace(strstr($number,substr($number,-3),' '),'.',-1,0)."万";
        }else{
            return $number;
        }
        return $str;
    }

    public function tupx(){
        $request = Request::instance();
        $params = $request->param();  /*获取参数*/
//        dump($params);exit;

        $AccountModel =  new AccountModel();
        $OrderModel = new OrderModel();

        $platform = $AccountModel->GetAccountRow();
        $sales_group = $AccountModel->GetSalesGroupRow();
        $time = $params['times'];
        $orderlist = $OrderModel->getOrder($params['platform_name'],$time);
        $time_list = $OrderModel->tiemsData($time);
        $aller = array();
        foreach ($time_list as $key=> $value){
            $aller[$key]['name'] = $value;
            $aller[$key]['sales_volume'] = 0;
            foreach ($orderlist as $ke=>$val){
                if($value == $val['payment_time']){
                    $aller[$key]['sales_volume'] = $val['sales_volume'];
                }
            }
        }

        //获取柱子
        $monthlyColumn = $OrderModel->getColumn2($time);
        unset($monthlyColumn[0]);
        $this->assign('monthlyColumn',$monthlyColumn);
//        饼状图
        $RefundModel = new RefundModel();
        $refList = $RefundModel->getRefund($time);
//        多表查询
        $orderSku = $OrderModel->getClassOrderSku($params['platform_name'],$time);
        $orderskuLists = [];
        foreach ($orderSku as $key=>$value){
            $orderskuLists[$key]['name'] = $value['name'];
            $orderskuLists[$key]['sales_volume'] = $value['sales_volume'];
            $orderskuLists[$key]['gross_profit'] = round(($value['sales_volume'] - $value['purchasing_cost'] - $value['platform_cost'] - $value['logistics_cost']),2);
            $orderskuLists[$key]['gross_profit_rate'] =  $value['sales_volume'] != 0 ?round(($orderskuLists[$key]['gross_profit'] / $value['sales_volume']) * 100,2):0;
        }
//        dump($orderskuLists);exit;
        $slalist = $OrderModel->sumSales($params['platform_name'],$time);
        $slalist['gross_profit'] = $slalist['sales_volume'] != 0 ? round(($slalist['sales_volume'] - $slalist['purchasing_cost'] - $slalist['platform_cost'] - $slalist['logistics_cost'])/10000,2):0;
        $slalist['platform_cost'] = $slalist['platform_cost'] != 0 ? round(floor($slalist['platform_cost']) / 10000, 2) : 0;
        $slalist['logistics_cost'] = $slalist['logistics_cost'] != 0 ? round(floor($slalist['logistics_cost']) / 10000, 2) : 0;
        $slalist['sales_volume'] = $slalist['sales_volume'] != 0 ? round(floor($slalist['sales_volume']) / 10000, 2) : 0;
        $slalist['purchasing_cost'] = $slalist['purchasing_cost'] != 0 ? round(floor($slalist['purchasing_cost']) / 10000, 2) : 0;
        $slalist['gross_profit_rate'] = $slalist['gross_profit'] != 0 ? round(($slalist['gross_profit'] / $slalist['sales_volume']) * 100,2) : 0;

//        dump($orderskuLists);exit;
        $this->assign('slalist',$slalist);
        $this->assign('orderskuLists',$orderskuLists);
        $this->assign('refList',json_encode(array_values($refList)));
        $this->assign('aller',$aller);
        $this->assign('sales_group',$sales_group);
        $this->assign('platform',$platform);

        return $this->fetch('index/pros');
    }
    //销售订单
    public function saleOrder(){

    }

    public function import(){

        return $this->fetch('index/import');
    }

    public function importfile(){

        if ($this->request->isPost()) {

            //接收参数
            $file = isset($_FILES['file']['name']) ? $_FILES['file']['name'] : '';


            if (!empty($file))
            {
                //设置文件后缀类型
                $str = array('.csv');
                //获取文件后缀名
                $suffixes = strrchr($file, '.');
                //判断后缀类型
                if(!in_array($suffixes, $str))
                {
                    die('文件类型不正确！请另存为.csv类型');
                }
            }
            else
            {
                die('没有选择上传文件！');
            }

            //设置PHP执行时间120秒
            ini_set("max_execution_time", "120");
            //行数
            $line_number = 0;
            $data_file = file($_FILES['file']['tmp_name']);

            $data = array();
            $type = input('get.type');
            if($type == 'zhanghu') {
                foreach ($data_file as $line) {
                    //跳过第一行
                    if ($line_number == 0) {
                        $line_number++;
                        continue;
                    }
                    $line_number++;
                    //转换编码
                    $line = $this->mult_iconv('GB2312', 'UTF-8', $line);

                    $arr = explode(',', trim($line));
                    if ($arr[0] == '') {
                        $line_number++;
                        continue;
                    }

                    $platform = $arr[0];
                    $alias = $arr[1];
                    $sales_group = $arr[2];

                    $data[] = array(
                        'platform' => $platform,
                        'alias' => $alias,
                        'sales_group' => $sales_group,
                    );
                }
                //检查是否有相同数据 过滤
                $datas = array();
                foreach ($data as $key => $value) {
                    if (!in_array($value, $datas)) {
                        $datas[$key] = $value;
                    }
                }
                if (empty($datas)) {
                    die("当前活动您导入的资料已经存在，无需再导入！");
                    exit;
                }
                //添加进数据库
                Db::table('test_account')->insertAll($datas);
            }elseif ($type == 'order')
            {
                foreach ($data_file as $line) {
                    //跳过第一行
                    if ($line_number == 0) {
                        $line_number++;
                        continue;
                    }
                    $line_number++;
                    //转换编码
                    $line = $this->mult_iconv('GB2312', 'UTF-8', $line);

                    $arr = explode(',', trim($line));
                    if ($arr[0] == '') {
                        $line_number++;
                        continue;
                    }

                    $platform = $arr[0];
                    $alias = $arr[1];
                    $code = $arr[2];
                    $number = $arr[3];
                    $order_no = $arr[4];
                    $sales_volume = $arr[5];
                    $purchasing_cost = $arr[6];
                    $platform_cost = $arr[7];
                    $logistics_cost = $arr[8];
                    $gross_profit = $arr[9];
                    $gross_profit_rate =  substr($arr[10],0,strlen($arr[10])-1);
                    $payment_time = $arr[11];

                    $data[] = array(
                        'platform' => $platform,
                        'alias' => $alias,
                        'code' => $code,
                        'number' => $number,
                        'order_no' => $order_no,
                        'sales_volume' => $sales_volume,
                        'purchasing_cost' => $purchasing_cost,
                        'platform_cost' => $platform_cost,
                        'logistics_cost' => $logistics_cost,
                        'gross_profit' => $gross_profit,
                        'gross_profit_rate' => $gross_profit_rate,
                        'payment_time' => $payment_time,
                    );
                }
                //检查是否有相同数据 过滤
                $datas = array();
                foreach ($data as $key => $value) {
                    if (!in_array($value, $datas)) {
                        $datas[$key] = $value;
                    }
                }
                if (empty($datas)) {
                    die("当前活动您导入的资料已经存在，无需再导入！");
                    exit;
                }
                //添加进数据库
                Db::table('test_order')->insertAll($datas);

            }elseif ($type == 'class'){
                foreach ($data_file as $line) {
                    //跳过第一行
                    if ($line_number == 0) {
                        $line_number++;
                        continue;
                    }
                    $line_number++;
                    //转换编码
                    $line = $this->mult_iconv('GB2312', 'UTF-8', $line);

                    $arr = explode(',', trim($line));
                    if ($arr[0] == '') {
                        $line_number++;
                        continue;
                    }

                    $sku = $arr[0];
                    $name = $arr[1];

                    $data[] = array(
                        'sku' => $sku,
                        'name' => $name,
                    );
                }
                //检查是否有相同数据 过滤
                $datas = array();
                foreach ($data as $key => $value) {
                    if (!in_array($value, $datas)) {
                        $datas[$key] = $value;
                    }
                }
                if (empty($datas)) {
                    die("当前活动您导入的资料已经存在，无需再导入！");
                    exit;
                }
                //添加进数据库
                Db::table('test_class')->insertAll($datas);
            }elseif ($type == 'refund'){

                foreach ($data_file as $line) {
                    //跳过第一行
                    if ($line_number == 0) {
                        $line_number++;
                        continue;
                    }
                    $line_number++;
                    //转换编码
                    $line = $this->mult_iconv('GB2312', 'UTF-8', $line);

                    $arr = explode(',', trim($line));
                    if ($arr[0] == '') {
                        $line_number++;
                        continue;
                    }

                    $reason = $arr[0];
                    $alias = $arr[1];
                    $order_no = $arr[2];
                    $money = $arr[3];
                    $time = $arr[4];
                    $platform = $arr[5];

                    $data[] = array(
                        'reason' => $reason,
                        'alias' => $alias,
                        'order_no' => $order_no,
                        'money' => $money,
                        'time' => $time,
                        'platform' => $platform,
                    );
                }
                //检查是否有相同数据 过滤
                $datas = array();
                foreach ($data as $key => $value) {
                    if (!in_array($value, $datas)) {
                        $datas[$key] = $value;
                    }
                }
                if (empty($datas)) {
                    die("当前活动您导入的资料已经存在，无需再导入！");
                    exit;
                }
                //添加进数据库
                Db::table('test_refund')->insertAll($datas);

            }

            die("数据导入完毕");
        }
    }

    /*数据编码转换*/
    public function mult_iconv($in_charset,$out_charset,$data)
    {
        if(substr($out_charset,-8)=='//IGNORE'){
            $out_charset=substr($out_charset,0,-8);
        }
        if(is_array($data)){
            foreach($data as $key => $value){
                if(is_array($value)){
                    $key=iconv($in_charset,$out_charset.'//IGNORE',$key);
                    $rtn[$key]=mult_iconv($in_charset,$out_charset,$value);
                }elseif(is_string($key) || is_string($value)){
                    if(is_string($key)){
                        $key=iconv($in_charset,$out_charset.'//IGNORE',$key);
                    }
                    if(is_string($value)){
                        $value=iconv($in_charset,$out_charset.'//IGNORE',$value);
                    }
                    $rtn[$key]=$value;
                }else{
                    $rtn[$key]=$value;
                }
            }
        }elseif(is_string($data)){
            $rtn=iconv($in_charset,$out_charset.'//IGNORE',$data);
        }else{
            $rtn=$data;
        }
        return $rtn;
    }

}
