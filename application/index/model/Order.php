<?php


namespace app\index\model;

use think\Db;
use think\Model;

class Order extends Model
{


    public function getOrder($platform = '', $payment_time = '', $group = '')
    {
        $where =[];
        if(!empty($platform)){
            $where['platform'] = $platform;
        }
        if(!empty($payment_time)){
            $where['payment_time'] = ['between',$this->getthemonth($payment_time)];
        }
        return Db::table('test_order')
            ->field('sum(sales_volume) as sales_volume,payment_time,platform')
            ->where($where)
            ->group('payment_time')
            ->select();
    }

    public function sumSales($platform = '', $payment_time = '', $group = ''){
        $where =[];
        if(!empty($platform)){
            $where['platform'] = $platform;
        }
        if(!empty($payment_time)){
            $where['payment_time'] = ['between',$this->getthemonth($payment_time)];
        }
        return Db::table('test_order')
            ->field('sum(sales_volume) as sales_volume,
            sum(purchasing_cost) as purchasing_cost,
            sum(platform_cost) as platform_cost,
            sum(logistics_cost) as logistics_cost,
             sum(gross_profit) as gross_profit')
            ->where($where)
            ->find();
    }
    /**
     * 获取柱子
     **/
    public function getColumn()
    {
        return $this->listTime('2020');
    }
    public function getColumn2($time){
        $starttime =date('Y',strtotime($time));
        return $this->listTime($starttime);
    }

    public function getClassOrderSku($platform='',$payment_time=''){
        $where =[];
        if(!empty($platform)){
            $where['order.platform'] = $platform;
        }
        if(!empty($payment_time)){
            $where['order.payment_time'] = ['between',$this->getthemonth($payment_time)];
        }

        return Db::table('test_order')
            ->field('sum(order.sales_volume) as sales_volume,
            sum(order.purchasing_cost) as purchasing_cost,
            sum(order.platform_cost) as platform_cost,
            sum(order.logistics_cost) as logistics_cost,
             class.name')
            ->where($where)
            ->alias('order')
            ->join('test_class class','order.code= class.sku')
            ->group('class.name')
            ->select();
    }

    public function tiemsData($payment_time)
    {
        $j = date('t', strtotime($payment_time . '-01'));
        $start_time = strtotime(date($payment_time . '-01')); //获取本月第一天时间戳
        $array = array();
        for ($i = 0; $i < $j; $i++) {
            $array[] = date('Y-m-d', $start_time + $i * 86400); //每隔一天赋值给数组
        }
        return $array;
    }

    public function listTime($y)
    {
        $month = $this->december($y);
        for ($i = 1; $i < count($month); ++$i) {
            $end_time = strtotime($month[$i]['time']) + 24 * 60 * 60 * 30 - 1;
            $ocount = Db::table('test_order')->
            where("payment_time", "between time", [strtotime($month[$i]['time']), $end_time])->sum('sales_volume');  //可变
            $month[$i]['payment'] = $ocount;
        }
        return $month;
    }


    public function december($years)
    {
        $lists_pf = [];
        for ($i = 0; $i <= 12; $i++) {
            $temp_time = [mktime(0, 0, 0, $i, 1, $years)];
            $time = date('Y-m', $temp_time[0]);
            $lists_pf[$i]['no'] = $i;
            $lists_pf[$i]['time'] = $time;
            $lists_pf[$i]['name'] = $i . "月";
            $lists_pf[$i]['payment'] = 0;
        }
        return $lists_pf;
    }


    function getthemonth($date)//获取具体日期的函数
    {
        $firstday = date('Y-m-01', strtotime($date));
        $lastday = date('Y-m-d', strtotime("$firstday +1 month -1day"));
        return array($firstday, $lastday);
    }
}